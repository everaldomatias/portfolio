jQuery(document).ready(function() {
	/*jQuery(".razao").niceScroll({
		cursorcolor:"#cccccc",
		cursorwidth:"1px",
		cursorborder:'none',
		scrollspeed: 100,
		cursoropacitymax: 0.75,
		mousescrollstep: 60
	});
	jQuery(".emocao").niceScroll({
		cursorcolor:"#cccccc",
		cursorwidth:"1px",
		cursorborder:'none',
		scrollspeed: 100,
		cursoropacitymax: 0.75,
		mousescrollstep: 60
	});
*/
	
	/*$(".razao, .emocao").hover(
	var screen = $(window);

	$(".razao, .emocao").hover(
		// If
		function(){

			if (screen.width() > 599) {

				$( this ).animate({
					opacity: 1,
				}, 200, function() {
				// Animation complete.
				});
			}

    	},
    	// Else
    	function(){

    		if (screen.width() > 599) {

	    		$( this ).animate({
					opacity: 0.75,
				}, 200, function() {
				// Animation complete.
				});
	    	}

		}
	);*/
});

var postsBtn = document.getElementById( 'posts-btn' );
var postsContainer = document.getElementById( 'posts-container' );
var body = document.querySelector( 'body' );
var blog = document.getElementById( 'blog' );
var blogRotating = document.getElementById( 'blog-rotating' );

if ( postsBtn ) {
    postsBtn.addEventListener( 'click', function(){

		var ourRequest = new XMLHttpRequest();
		
		blogRotating.style.display = 'inline-block';
		
        ourRequest.open( 'GET', 'https://everaldo.dev/wp/wp-json/wp/v2/posts?order=desc' );
        ourRequest.onload = function() {
            if ( ourRequest.status >= 200 && ourRequest.status < 400 ) {
				var data = JSON.parse( ourRequest.responseText );
				//console.log(data)
				createHTML( data );
				runCSS();
            } else {
                console.log( 'We connected to the server, but it returned an error.' );
            }
        };

        ourRequest.onerror = function() {
            console.log( 'Connection error' );
        }

		ourRequest.send();

    });
}
function runCSS() {
	setTimeout( function(){
		body.classList.add( 'show-blog' );
	  	blog.scrollIntoView({
			block:    'start'
	   });
	}, 200 );
}
function createHTML( postsData ) {
    var ourHTMLString = '';
    
    for( var i = 0; i < postsData.length; i++ ) {
		ourHTMLString += '<div class="each-post">';
        ourHTMLString += '<h2>' + postsData[i].title.rendered + '</h2>';
		ourHTMLString += postsData[i].content.rendered;
		ourHTMLString += '</div><!-- /.each-post -->';
    }

    postsContainer.innerHTML = ourHTMLString;
}
